C:/VulkanSDK/1.1.130.0/Bin32/glslc.exe shader.vert -o shader.vert.spv
C:/VulkanSDK/1.1.130.0/Bin32/glslc.exe shader.frag -o shader.frag.spv

xcopy /y "shader.vert.spv" "../bin/Debug/shaders/shader.vert.spv"
xcopy /y "shader.frag.spv" "../bin/Debug/shaders/shader.frag.spv"
xcopy /y "shader.vert.spv" "../bin/Release/shaders/shader.vert.spv"
xcopy /y "shader.frag.spv" "../bin/Release/shaders/shader.frag.spv"
pause