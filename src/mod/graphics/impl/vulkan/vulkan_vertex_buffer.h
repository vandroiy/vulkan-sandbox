#pragma once 

#include "vulkan_includes.h"

class VulkanVertexBuffer
{
public:
    void init(VkPhysicalDevice, VkDevice, VkQueue, VkCommandPool);
    void deinit(VkDevice);

    VkBuffer get_vertex_buffer() const;
    VkBuffer get_index_buffer() const;

    uint32_t get_vertex_count() const;
    uint32_t get_indices_count() const;

private:
    const std::vector<vertex> vertices = {
        {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
        {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
        {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
        {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
    };

    const std::vector<uint16_t> indices = {
        0, 1, 2, 2, 3, 0
    };

    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;

    void create_vertex_buffer(VkPhysicalDevice, VkDevice, VkQueue, VkCommandPool, OUT VkBuffer&);
    void create_index_buffer(VkPhysicalDevice, VkDevice, VkQueue, VkCommandPool, OUT VkBuffer&);
};