#pragma once 

#include "vulkan_includes.h"

#define VK_CHECK(x) \
	do \
	{ \
		VkResult err = x; \
		if (err) \
		{ \
			std::cout << "Detected Vulkan error " << int(err) << " at " << __FILE__ << ":" << __LINE__ << std::endl; \
			abort(); \
		} \
	} while (0)

#define VK_CHECK_RETBOOL(x) \
	do \
	{ \
		VkResult err = x; \
		if (err) \
		{ \
			std::cout << "Detected Vulkan error " << int(err) << " at " << __FILE__ << ":" << __LINE__ << std::endl; \
			return false; \
		} \
	} while (0)

#define VK_CHECK_RETNULL(x) \
	do \
	{ \
		VkResult err = x; \
		if (err) \
		{ \
			std::cout << "Detected Vulkan error " << int(err) << " at " << __FILE__ << ":" << __LINE__ << std::endl; \
			return nullptr; \
		} \
	} while (0)

#define C_ARR_SIZE_UINT32(x) static_cast<uint32_t>(sizeof(x) / sizeof(x[0]))  
#define OUT /*OUT*/

struct vulkan_utils
{
    constexpr static char Important_Console_Header[] =
"    __      __         _     _ _                       \n \
   \\ \\    / /        (_)   | (_)                      \n \
    \\ \\  / __ _ _ __  _  __| |_ _   _ _ __ ___        \n \
     \\ \\/ / _` | '_ \\| |/ _` | | | | | '_ `  _\\     \n \
      \\  | (_| | | | | | (_| | | |_| | | | | | |       \n \
       \\/ \\__,_|_| |_|_|\\__,_|_|\\__,_|_| |_| |_|\n\n";

    static std::vector<char> read_file(const std::string& path)
    {
        std::ifstream file(path, std::ios::ate | std::ios::binary);
        if (!file.is_open())
        {
            VK_CHECK(VK_INCOMPLETE);
            return std::vector<char>(0);
        }

        size_t size = static_cast<size_t>(file.tellg());
        std::vector<char> buffer(size);

        file.seekg(0);
        file.read(buffer.data(), size);
        file.close();

        std::cout << "Read shader: " << path.c_str() << std::endl;

        return buffer;
    }       
    
    static std::set<std::string> get_supported_extensions()
    {
        uint32_t count;
        vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr); //get number of extensions
        std::vector<VkExtensionProperties> extensions(count);
        vkEnumerateInstanceExtensionProperties(nullptr, &count, extensions.data()); //populate buffer

        std::set<std::string> results;
        for (auto& extension : extensions) {
            results.insert(extension.extensionName);
        }

        return results;
    }

    static std::vector<VkPhysicalDevice> get_physical_devices(const VkInstance instance)
    {
        uint32_t count;
        vkEnumeratePhysicalDevices(instance, &count, nullptr);

        std::vector<VkPhysicalDevice> devices(count);
        vkEnumeratePhysicalDevices(instance, &count, devices.data());

        return devices;
    }

    static void print_info(const VkInstance instance, VkPhysicalDeviceProperties primaryGpuProps)
    {
        std::cout << Important_Console_Header << std::endl;
        
        const auto extensions = get_supported_extensions();
        std::cout << "Extensions: " << extensions.size() << " supported" << std::endl;
        for (const auto& extension : extensions)
        {
            std::cout << extension << std::endl;
        }

        std::cout << std::endl;
        const auto devices = get_physical_devices(instance);
        std::cout << "Physical devices: " << devices.size() << std::endl;

        for (const auto& device : devices)
        {
            VkPhysicalDeviceProperties properties{};
            vkGetPhysicalDeviceProperties(device, &properties);

            VkPhysicalDeviceMemoryProperties memoryProperties{};
            vkGetPhysicalDeviceMemoryProperties(device, &memoryProperties);


            std::cout << "[" << (primaryGpuProps.deviceID == properties.deviceID ? "X" : " ") << "] ";
            
            std::cout << properties.deviceName << std::endl;
            std::cout << "\tvendor id: " << properties.vendorID << std::endl;
            std::cout << "\ttype: " << print_device_type(properties.deviceType).c_str() << std::endl;
            std::cout << "\tapi version: " << properties.apiVersion << std::endl;
            std::cout << "\tdriver version: " << properties.driverVersion << std::endl;
            std::cout << "\tmemory:" << std::endl;
            for (uint32_t i = 0; i < memoryProperties.memoryHeapCount; i++)
            {
                std::cout << "\t\theap " << i << ": " << (memoryProperties.memoryHeaps[i].size / 1024 / 1024 / 1024) << "gb" << std::endl;
            }
           
            std::cout << std::endl;
        }

    }

    static std::string print_device_type(VkPhysicalDeviceType type)
    {
        std::string result;

        switch (type)
        {
            case VK_PHYSICAL_DEVICE_TYPE_OTHER: { result = "other"; } break;
            case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: { result = "integrated"; } break;
            case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: { result = "discrete"; } break;
            case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: { result = "virtual"; } break;
            case VK_PHYSICAL_DEVICE_TYPE_CPU: { result = "cpu"; } break;
        }

        return result;
    }

    static void print_error_reason(const VkResult* result)
    {
        if (result != nullptr)
        {
            switch (*result)
            {
            case VK_ERROR_INCOMPATIBLE_DRIVER:
            {
                std::cout << "Incompatible driver!" << std::endl;
            } break;
            default:
            {
                std::cout << "Unknown error!" << std::endl;
            }
            }
        }
    }
};