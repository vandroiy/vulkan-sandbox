#pragma once

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <set>
#include <vector>
#include <string>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <optional>
#include <memory>
#include <chrono>

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#ifndef GLM_FORCE_RADIANS
	#define GLM_FORCE_RADIANS
#endif
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "vulkan_utils.h"

#include "./util/buffers.h"
#include "./util/uniform_buffer_object.h"
#include "./util/vertex.h"
