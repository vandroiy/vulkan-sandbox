#include "vulkan_renderer.h"
#include "vulkan_window.h"

VulkanRenderer::VulkanRenderer()
{
	vkhInstance = VK_NULL_HANDLE;
	vkhGPU = VK_NULL_HANDLE;
	vkhDevice = VK_NULL_HANDLE;
	vkhKhrSurface = VK_NULL_HANDLE;
}

VulkanRenderer::~VulkanRenderer()
{
	vkDestroyCommandPool(vkhDevice, commandPool, nullptr);
	vkDestroyDevice(vkhDevice, nullptr);

	if (vkhInstance != nullptr)
	{
		vkDestroySurfaceKHR(vkhInstance, VkSurfaceKHR(vkhKhrSurface), nullptr);
		vkDestroyInstance(vkhInstance, nullptr);
	}
}

void
VulkanRenderer::on_window_change(GLFWwindow* window)
{
	/* minimize */ 
	/*int width = 0, height = 0;
	glfwGetFramebufferSize(window, &width, &height);
	while (width == 0 || height == 0) {
		glfwGetFramebufferSize(window, &width, &height);
		glfwWaitEvents();
	}*/

	/* resize */
	vkDeviceWaitIdle(vkhDevice);

	on_window_change_cleanup();

	swapchainWrapper.recreate(window, vkhGPU, vkhDevice, vkhKhrSurface, &queueWrapper);
	pipelineWrapper.recreate(vkhDevice, &swapchainWrapper);
	update_command_buffers();
}

void VulkanRenderer::on_window_change_cleanup()
{
	swapchainWrapper.recreate_cleanup(vkhDevice);
	pipelineWrapper.recreate_cleanup(vkhDevice, &swapchainWrapper);

	vkFreeCommandBuffers(vkhDevice, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
	vkDestroyCommandPool(vkhDevice, commandPool, nullptr);
}

VkPhysicalDevice
VulkanRenderer::find_suitable_gpu(const std::vector<VkPhysicalDevice> devices) const
{
	if (devices.size() <= 0) {
		return nullptr;
	}

	VkPhysicalDevice result = devices[0]; // fallback

	for (const auto& device : devices)
	{
		VkPhysicalDeviceProperties properties;
		vkGetPhysicalDeviceProperties(device, &properties);
		if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && 
			does_gpu_support_req_extensions(device))
		{
			result = device;
			break;
		}
	}

	return result;
}

bool
VulkanRenderer::does_gpu_support_req_extensions(VkPhysicalDevice physicalDevice) const
{
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, availableExtensions.data());
	
	std::set<std::string> requiredExtensions(vpDeviceExtensions.begin(), vpDeviceExtensions.end());

	for (const auto& extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();
}

bool
VulkanRenderer::init(GLFWwindow* window)
{
	//todo: make this better pls
	auto result =
		init_debug_layers() &&
		init_instance(window) &&
		init_devices() &&
		init_command_pool();

	swapchainWrapper.init(window, vkhGPU, vkhDevice, vkhKhrSurface, &queueWrapper);
	pipelineWrapper.init(vkhDevice, &swapchainWrapper);
	syncWrapper.init(vkhDevice, &swapchainWrapper);
	vertexBuffer.init(vkhGPU, vkhDevice, queueWrapper.graphicsQueue, commandPool);
	uniformBuffer.init(vkhGPU, vkhDevice, queueWrapper.graphicsQueue, commandPool);

	result = result && update_command_buffers();

	if (result)
	{
		vulkan_utils::print_info(vkhInstance, gpuProperties);
	}
	
	return result;
}

bool
VulkanRenderer::init_instance(GLFWwindow* window)
{
	// get the required extensions list
	uint32_t requiredExtensionsCount = 0;
	auto requiredExtensionsNames = glfwGetRequiredInstanceExtensions(&requiredExtensionsCount);

	for (uint32_t i = 0; i < requiredExtensionsCount; ++i)
	{
		vpInstanceExtensions.push_back(requiredExtensionsNames[i]);
	}
	
	// prepare for vulkan instance creation
	VkApplicationInfo applicationInfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO  };
	applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 2);
	applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	applicationInfo.pApplicationName = "Vulkan";

	VkInstanceCreateInfo instanceCreateInfo = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO  };
	instanceCreateInfo.flags = 0;
	instanceCreateInfo.pApplicationInfo = &applicationInfo;
	instanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(vpInstanceExtensions.size());
	instanceCreateInfo.ppEnabledExtensionNames = vpInstanceExtensions.data();
	instanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(vpInstanceLayers.size());
	instanceCreateInfo.ppEnabledLayerNames= vpInstanceLayers.data();
	
	// ~INSTANCE
	VK_CHECK_RETBOOL(vkCreateInstance(&instanceCreateInfo, nullptr, &vkhInstance));
	
	// ~KHRSURFACE
	VK_CHECK_RETBOOL(glfwCreateWindowSurface(vkhInstance, window, nullptr, &vkhKhrSurface));

	return true;
}

bool
VulkanRenderer::init_devices()
{
	// ~DEVICES
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(vkhInstance, &deviceCount, nullptr);

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(vkhInstance, &deviceCount, devices.data());

	vkhGPU = find_suitable_gpu(devices);
	vkGetPhysicalDeviceProperties(vkhGPU, &gpuProperties);
	
	if (!queueWrapper.init(vkhGPU, vkhDevice, vkhKhrSurface))
	{
		std::cout << "Selected GPU doesn't support graphics and/or presentation queues" << std::endl;
		return false;
	}

	VkDeviceCreateInfo deviceCreateInfo {};
	std::memset(&deviceCreateInfo, 0, sizeof(deviceCreateInfo));
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueWrapper.queueCreateInfos.size());
	deviceCreateInfo.pQueueCreateInfos = queueWrapper.queueCreateInfos.data();
	deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(vpDeviceExtensions.size());
	deviceCreateInfo.ppEnabledExtensionNames = vpDeviceExtensions.data();

	VK_CHECK_RETBOOL(vkCreateDevice(vkhGPU, &deviceCreateInfo, nullptr, &vkhDevice));

	queueWrapper.find_queue_handles(vkhDevice);

	return true;
}

bool
VulkanRenderer::init_debug_layers()
{
	if (VulkanWindow::DEBUG)
	{
		vpInstanceLayers.push_back("VK_LAYER_KHRONOS_validation");
	}
	
	return true;
}

bool
VulkanRenderer::init_command_pool()
{
	VkCommandPoolCreateInfo commandPoolCreateInfo{ VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
	commandPoolCreateInfo.queueFamilyIndex = queueWrapper.queueFamilyIndices.graphicsFamily.value();

	VK_CHECK_RETBOOL(vkCreateCommandPool(vkhDevice, &commandPoolCreateInfo, nullptr, &commandPool));
	return true;
}

bool
VulkanRenderer::update_command_buffers()
{
	init_command_pool();

	commandBuffers.resize(swapchainWrapper.swapchainFramebuffers.size());
	VkCommandBufferAllocateInfo commandBufferAllocateInfo{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

	vkAllocateCommandBuffers(vkhDevice, &commandBufferAllocateInfo, commandBuffers.data());

	for (size_t i = 0; i < commandBuffers.size(); i++) {
		VkCommandBufferBeginInfo commandBufferBeginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO  };

		VK_CHECK(vkBeginCommandBuffer(commandBuffers[i], &commandBufferBeginInfo));

			VkRenderPassBeginInfo renderPassBeginInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO };
			renderPassBeginInfo.renderPass = pipelineWrapper.renderPass;
			renderPassBeginInfo.framebuffer = swapchainWrapper.swapchainFramebuffers[i];
			renderPassBeginInfo.renderArea.offset = { 0, 0 };
			renderPassBeginInfo.renderArea.extent = swapchainWrapper.supportDetails.swapExtent;

			VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };

			renderPassBeginInfo.clearValueCount = 1;
			renderPassBeginInfo.pClearValues = &clearColor;

			vkCmdBeginRenderPass(commandBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
				vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineWrapper.graphicsPipeline);
				
				// !!
				VkBuffer vertexBuffers[] = { vertexBuffer.get_vertex_buffer() }; // we only have one at the moment
				VkDeviceSize offsets[] = { 0 };
				uint32_t vertexCount = vertexBuffer.get_vertex_count(); // todo: we could have multiple vertex buffers, so std::accumulate

				vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);
				vkCmdBindIndexBuffer(commandBuffers[i], vertexBuffer.get_index_buffer(), 0, VK_INDEX_TYPE_UINT16);

				vkCmdDrawIndexed(commandBuffers[i], vertexBuffer.get_indices_count(), 1, 0, 0, 0);
				// !!

			vkCmdEndRenderPass(commandBuffers[i]);

		VK_CHECK(vkEndCommandBuffer(commandBuffers[i]));
	}

	return true;
}

bool
VulkanRenderer::render(GLFWwindow* hWnd)
{
	while (!glfwWindowShouldClose(hWnd))
	{
		glfwPollEvents();

		/* renderframe starts here */
		vkWaitForFences(vkhDevice, 1, &syncWrapper.get_free_fence(), VK_TRUE, UINT64_MAX);

		uint32_t imageIndex;
		VkResult nextImageResult = vkAcquireNextImageKHR(vkhDevice, swapchainWrapper.swapchain, UINT64_MAX, syncWrapper.get_free_image_available_semaphore(), VK_NULL_HANDLE, &imageIndex);

		if (nextImageResult == VK_ERROR_OUT_OF_DATE_KHR)
		{
			on_window_change(hWnd);
			return false;
		}

		// update the uniform buffers
		uniformBuffer.update_uniform_buffer(vkhDevice, imageIndex, swapchainWrapper.supportDetails.swapExtent);

		// is this currently in use?
		if (syncWrapper.fencesInUse[imageIndex] != VK_NULL_HANDLE) {
			// then we wait
			vkWaitForFences(vkhDevice, 1, &syncWrapper.fencesInUse[imageIndex], VK_TRUE, UINT64_MAX);
		}

		// mark it for use
		syncWrapper.fencesInUse[imageIndex] = syncWrapper.get_free_fence();

		VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO };

		VkSemaphore waitSemaphores[] = { syncWrapper.get_free_image_available_semaphore() };
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

		VkSemaphore signalSemaphores[] = { syncWrapper.get_free_render_finished_semaphore() };
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		// reset fences
		vkResetFences(vkhDevice, 1, &syncWrapper.get_free_fence());

		VK_CHECK(vkQueueSubmit(queueWrapper.graphicsQueue, 1, &submitInfo, syncWrapper.get_free_fence()));

		VkSubpassDependency subpassDependency = {};
		subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		subpassDependency.dstSubpass = 0;
		subpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		subpassDependency.srcAccessMask = 0;
		subpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		subpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = signalSemaphores;

		VkSwapchainKHR swapChains[] = { swapchainWrapper.swapchain };
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = swapChains;
		presentInfo.pImageIndices = &imageIndex;
		presentInfo.pResults = nullptr;
		
		VkResult presentResult = vkQueuePresentKHR(queueWrapper.presentQueue, &presentInfo);
		if (presentResult == VK_ERROR_OUT_OF_DATE_KHR || swapchainWrapper.framebufferResized)
		{
			swapchainWrapper.framebufferResized = false;
			on_window_change(hWnd);
		}

		/* renderframe ends here */

		syncWrapper.advance_frame_counter();
	}
	
	return true;
}