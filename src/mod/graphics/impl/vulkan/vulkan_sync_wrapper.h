#pragma once

#include "vulkan_includes.h"

#include "vulkan_swapchain_wrapper.h"

class VulkanSyncWrapper
{
public:
	static constexpr size_t MAX_CONCURRENT_FRAMES = 2;

	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> fences;
	std::vector<VkFence> fencesInUse;

	size_t currentFrame{ 0 };

	void init(VkDevice,VulkanSwapchainWrapper*);
	void deinit(VkDevice);

	void advance_frame_counter();
	VkSemaphore& get_free_image_available_semaphore();
	VkSemaphore& get_free_render_finished_semaphore();
	VkFence& get_free_fence();
};