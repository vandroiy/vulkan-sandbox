#pragma once

#include "vulkan_includes.h"
#include "vulkan_renderer.h"

class VulkanWindow
{
public:
	static constexpr int WINDOW_WIDTH = 800;
	static constexpr int WINDOW_HEIGHT = 600;
	static constexpr char WINDOW_TITLE[] = "Vulkan";
	static bool DEBUG;

	VulkanRenderer* pRenderer;
	GLFWwindow* hWnd;
	
	VulkanWindow();
	~VulkanWindow();

	bool init();
	void render();
};