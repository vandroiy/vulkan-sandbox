#include "vulkan_vertex_buffer.h"

#include <vulkan/vulkan.h>

void
VulkanVertexBuffer::init(VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool) 
{
   create_vertex_buffer(physicalDevice, device, graphicsQueue, commandPool, vertexBuffer);
   create_index_buffer(physicalDevice, device, graphicsQueue, commandPool, indexBuffer);
}

void
VulkanVertexBuffer::deinit(VkDevice device) {
    vkDestroyBuffer(device, indexBuffer, nullptr);
    vkFreeMemory(device, indexBufferMemory, nullptr);

    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, vertexBufferMemory, nullptr);
}

void
VulkanVertexBuffer::create_vertex_buffer(VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    OUT VkBuffer& vertexBuffer)
{
    VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    VulkanBufferUtils::create_buffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer,
        stagingBufferMemory
    );

    // fill the vertex buffer with data by mapping it to
    // a temporary pointer, copying the data, and unmapping
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    std::memcpy(data, vertices.data(), (size_t)bufferSize);
    vkUnmapMemory(device, stagingBufferMemory);

    // create the vertex buffer
    VulkanBufferUtils::create_buffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        vertexBuffer,
        vertexBufferMemory);

    // copy the contents of the staging buffer to the vertex buffer
    VulkanBufferUtils::copy_buffer(device, graphicsQueue, commandPool, stagingBuffer, vertexBuffer, bufferSize);

    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void
VulkanVertexBuffer::create_index_buffer(VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    OUT VkBuffer& indexBuffer)
{
    VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    VulkanBufferUtils::create_buffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer,
        stagingBufferMemory
    );

    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    std::memcpy(data, indices.data(), (size_t)bufferSize);
    vkUnmapMemory(device, stagingBufferMemory);

    VulkanBufferUtils::create_buffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        indexBuffer,
        indexBufferMemory
    );

    VulkanBufferUtils::copy_buffer(
        device,
        graphicsQueue,
        commandPool,
        stagingBuffer,
        indexBuffer,
        bufferSize
    );

    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

VkBuffer
VulkanVertexBuffer::get_vertex_buffer() const 
{
    return vertexBuffer;
}

VkBuffer
VulkanVertexBuffer::get_index_buffer() const 
{
    return indexBuffer;
}

uint32_t
VulkanVertexBuffer::get_vertex_count() const 
{
    return static_cast<uint32_t>(vertices.size());
}

uint32_t
VulkanVertexBuffer::get_indices_count() const 
{
    return static_cast<uint32_t>(indices.size());
}