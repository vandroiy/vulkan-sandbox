#pragma once 

#include "../vulkan_includes.h"
#include <vulkan/vulkan.h>

struct VulkanBufferUtils
{
    static void
    create_buffer(VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkDeviceSize size,
        VkBufferUsageFlags usageFlags,
        VkMemoryPropertyFlags memoryPropertyFlags,
        OUT VkBuffer& outBuffer,
        OUT VkDeviceMemory& bufferMemory) 
    {
        // create the buffeefr
        VkBufferCreateInfo bufferCreateInfo{ VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        bufferCreateInfo.size = size;
        bufferCreateInfo.usage = usageFlags;
        bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VK_CHECK(vkCreateBuffer(device, &bufferCreateInfo, nullptr, &outBuffer));

        // get requirements for the buffer and find the suitable memory to store it in
        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(device, outBuffer, &memoryRequirements);

        auto suitableMemoryType = VulkanBufferUtils::find_suitable_memory_type(
            physicalDevice,
            size,
            memoryRequirements.memoryTypeBits,
            memoryPropertyFlags);

        // allocate the memory for the buffer
        VkMemoryAllocateInfo memoryAllocateInfo{ VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
        memoryAllocateInfo.allocationSize = memoryRequirements.size;
        memoryAllocateInfo.memoryTypeIndex = suitableMemoryType;

        VK_CHECK(vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &bufferMemory));
        std::cout << "Allocated " << size << " bytes. !! TODO: log host/device and for what purpose !!" << std::endl;

        // bind the allocated memory to the buffer
        vkBindBufferMemory(device, outBuffer, bufferMemory, 0);
    }

    static void
    copy_buffer(VkDevice device,
        VkQueue graphicsQueue,
        VkCommandPool commandPool,
        VkBuffer srcBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize size) 
    {
        // allocate a command buffer
        VkCommandBufferAllocateInfo commandBufferAllocateInfo{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandPool = commandPool;
        commandBufferAllocateInfo.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);

        // record
        VkCommandBufferBeginInfo commandBufferBeginInfo { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        // copy
        VkBufferCopy bufferRegionCopy;
        bufferRegionCopy.srcOffset = 0;
        bufferRegionCopy.dstOffset = 0;
        bufferRegionCopy.size = size;

        vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
        vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &bufferRegionCopy);
        vkEndCommandBuffer(commandBuffer);

        // submit it 
        VkSubmitInfo submitInfo { VK_STRUCTURE_TYPE_SUBMIT_INFO };
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(graphicsQueue);

        vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
    }

    // TODO: sooner or later we will need a memory.h
    static uint32_t
    find_suitable_memory_type(VkPhysicalDevice physicalDevice,
        VkDeviceSize size,
        uint32_t typeFilter,
        VkMemoryPropertyFlags properties)
    {
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
        {
            if ((typeFilter & (1 << i)) &&
                (memoryProperties.memoryTypes[i].propertyFlags & properties) == properties) {
                return i;
            }
        }

        std::cout << "Failed to find suitable memory type for vertex buffer (" << size << " bytes)" << std::endl;
        abort();
    }

};