#include "vulkan_uniform_buffer.h"

#include <vulkan/vulkan.h>

void
VulkanUniformBuffer::init(VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool) 
{
    VkBuffer fakeBuffer;
    create_uniform_buffer(physicalDevice, device, graphicsQueue, commandPool, fakeBuffer);
}

void
VulkanUniformBuffer::deinit(VkDevice device) {

    for (size_t i = 0; i < swapchainImageCount; i++) {
        vkDestroyBuffer(device, uniformBuffers[i], nullptr);
        vkFreeMemory(device, uniformBuffersMemory[i], nullptr);
    }
}

void
VulkanUniformBuffer::create_uniform_buffer(VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    OUT VkBuffer& uniformBuffer)
{
    VkDeviceSize bufferSize = sizeof(UniformBufferObject);
    uniformBuffers.resize(swapchainImageCount);
    uniformBuffersMemory.resize(swapchainImageCount);

    for (size_t i = 0; i < swapchainImageCount; i++) {
        VulkanBufferUtils::create_buffer(
            physicalDevice,
            device,
            bufferSize,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            uniformBuffers[i],
            uniformBuffersMemory[i]
        );
    }
}

void
VulkanUniformBuffer::update_uniform_buffer(VkDevice device, uint32_t imageIndex, const VkExtent2D& swapchainExtent)
{
    static auto startTime = std::chrono::high_resolution_clock::now();
    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    UniformBufferObject ubo{};
    ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    ubo.projection = glm::perspective(glm::radians(45.0f), swapchainExtent.width / (float) swapchainExtent.height, 0.1f, 10.0f);

    ubo.projection[1][1] *= -1;

    void* data;
    vkMapMemory(device, uniformBuffersMemory[imageIndex], 0, sizeof(ubo), 0, &data);
    std::memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(device, uniformBuffersMemory[imageIndex]);
}