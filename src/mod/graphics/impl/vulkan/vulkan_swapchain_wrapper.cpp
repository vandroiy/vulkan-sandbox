#include "vulkan_swapchain_wrapper.h"


VulkanSwapchainWrapper::SwapchainSupportDetails
VulkanSwapchainWrapper::query_swapchain_support(GLFWwindow* window, VkPhysicalDevice device, VkSurfaceKHR surface) {
	SwapchainSupportDetails details;

	VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities));
	
	details.format = details.get_best_surface_format(device, surface);
	details.presentMode = details.get_best_present_mode(device, surface);
	details.swapExtent = details.get_swap_extent(window);

	return details;
}

VkSurfaceFormatKHR
VulkanSwapchainWrapper::SwapchainSupportDetails::get_best_surface_format(VkPhysicalDevice gpu, VkSurfaceKHR surface)
{
	uint32_t surfaceFormatCount;
	VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &surfaceFormatCount, nullptr));
	std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatCount);
	VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &surfaceFormatCount, surfaceFormats.data()));

	for (const auto& surfaceFormat : surfaceFormats)
	{
		if (surfaceFormat.format == VK_FORMAT_B8G8R8A8_UNORM &&
			surfaceFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
		{
			std::cout << "Using format: VK_FORMAT_B8G8R8A8_UNORM ; color_space: VK_COLOR_SPACE_SRGB_NONLINEAR_KHR" << std::endl;
			return surfaceFormat;
		}
	}

	return surfaceFormats[0];
}

VkPresentModeKHR 
VulkanSwapchainWrapper::SwapchainSupportDetails::get_best_present_mode(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);
	std::vector<VkPresentModeKHR> presentModes(presentModeCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, presentModes.data());

	for (const auto& presentMode : presentModes) {
		if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			std::cout << "Using present mode: VK_PRESENT_MODE_MAILBOX_KHR" << std::endl;
			return presentMode;
		}
	}

	std::cout << "Using present mode: VK_PRESENT_MODE_FIFO_KHR" << std::endl;
	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D
VulkanSwapchainWrapper::SwapchainSupportDetails::get_swap_extent(GLFWwindow* window)
{
	if (capabilities.currentExtent.width != UINT32_MAX)
	{
		return capabilities.currentExtent;
	}

	int width = 0, height = 0;
	glfwGetWindowSize(window, &width, &height);

	VkExtent2D actualExtent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };
	actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
	actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

	return actualExtent;
}

void
VulkanSwapchainWrapper::init(GLFWwindow* window, VkPhysicalDevice gpu, VkDevice device, VkSurfaceKHR surface, VulkanQueueWrapper* queueWrapper)
{
	supportDetails = query_swapchain_support(window, gpu, surface);

	create_swapchains(window, gpu, device, surface, queueWrapper);
	create_images(device);
	create_image_views(device);
}

void
VulkanSwapchainWrapper::recreate(GLFWwindow* window, VkPhysicalDevice gpu, VkDevice device, VkSurfaceKHR surface, VulkanQueueWrapper* queueWrapper)
{
	supportDetails = query_swapchain_support(window, gpu, surface);

	create_swapchains(window, gpu, device, surface, queueWrapper);
	create_images(device);
	create_image_views(device);
}

void
VulkanSwapchainWrapper::recreate_cleanup(VkDevice device)
{
	for (size_t i = 0; i < swapchainFramebuffers.size(); i++)
	{
		vkDestroyFramebuffer(device, swapchainFramebuffers[i], nullptr);
	}

	for (size_t i = 0; i < swapchainImageViews.size(); i++)
	{
		vkDestroyImageView(device, swapchainImageViews[i], nullptr);
	}

	vkDestroySwapchainKHR(device, swapchain, nullptr);
}

void
VulkanSwapchainWrapper::deinit(VkDevice device)
{
	for (auto framebuffer : swapchainFramebuffers)
	{
		vkDestroyFramebuffer(device, framebuffer, nullptr);
	}
}

void
VulkanSwapchainWrapper::create_swapchains(GLFWwindow* window, VkPhysicalDevice gpu, VkDevice device, VkSurfaceKHR surface, VulkanQueueWrapper* queueWrapper)
{
	int width = 0, height = 0;
	glfwGetWindowSize(window, &width, &height);

	VkBool32 presentSupport = false;
	vkGetPhysicalDeviceSurfaceSupportKHR(gpu, queueWrapper->queueFamilyIndices.graphicsFamily.value(), surface, &presentSupport);

	VkSwapchainCreateInfoKHR swapchainCreateInfo{ VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };
	swapchainCreateInfo.surface = surface;
	swapchainCreateInfo.minImageCount = 2;
	swapchainCreateInfo.imageFormat = supportDetails.format.format;
	swapchainCreateInfo.imageColorSpace = supportDetails.format.colorSpace;
	swapchainCreateInfo.imageExtent.width = width;
	swapchainCreateInfo.imageExtent.height = height;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCreateInfo.queueFamilyIndexCount = 1;
	swapchainCreateInfo.pQueueFamilyIndices = &(queueWrapper->queueFamilyIndices.graphicsFamily.value());
	swapchainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
	swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

	VK_CHECK(vkCreateSwapchainKHR(device, &swapchainCreateInfo, 0, &swapchain));
}

void
VulkanSwapchainWrapper::create_images(VkDevice device)
{
	uint32_t swapchainImageCount;
	VK_CHECK(vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, nullptr));
	swapchainImages.resize(swapchainImageCount);
	vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, swapchainImages.data());
}

void
VulkanSwapchainWrapper::create_image_views(VkDevice device)
{
	swapchainImageViews.resize(swapchainImages.size());
	for (size_t i = 0; i < swapchainImages.size(); ++i)
	{
		VkImageViewCreateInfo imageViewCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
		imageViewCreateInfo.image = swapchainImages[i];
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = supportDetails.format.format;
		imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;

		VK_CHECK(vkCreateImageView(device, &imageViewCreateInfo, nullptr, &swapchainImageViews[i]));
	}
}