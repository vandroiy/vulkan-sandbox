#pragma once

#include "vulkan_includes.h"
#include "vulkan_queue_wrapper.h"

class VulkanSwapchainWrapper
{
public:
	class SwapchainSupportDetails 
	{
	public:
		VkSurfaceCapabilitiesKHR capabilities;
		VkSurfaceFormatKHR format;
		VkPresentModeKHR presentMode;
		VkExtent2D swapExtent;

		VkSurfaceFormatKHR get_best_surface_format(VkPhysicalDevice, VkSurfaceKHR);
		VkPresentModeKHR get_best_present_mode(VkPhysicalDevice, VkSurfaceKHR);
		VkExtent2D get_swap_extent(GLFWwindow*);
	};
	SwapchainSupportDetails supportDetails;
	
	SwapchainSupportDetails query_swapchain_support(GLFWwindow*, VkPhysicalDevice, VkSurfaceKHR);
	
	VkSwapchainKHR swapchain;
	std::vector<VkImage> swapchainImages;
	std::vector<VkImageView> swapchainImageViews;
	std::vector<VkFramebuffer> swapchainFramebuffers;

	bool framebufferResized{ false };

	void init(GLFWwindow*, VkPhysicalDevice, VkDevice, VkSurfaceKHR, VulkanQueueWrapper*);
	void deinit(VkDevice);

	void recreate(GLFWwindow* window, VkPhysicalDevice gpu, VkDevice device, VkSurfaceKHR surface, VulkanQueueWrapper* queueWrapper);
	void recreate_cleanup(VkDevice);

	void create_swapchains(GLFWwindow*, VkPhysicalDevice, VkDevice, VkSurfaceKHR, VulkanQueueWrapper*);
	void create_images(VkDevice);
	void create_image_views(VkDevice);

};
