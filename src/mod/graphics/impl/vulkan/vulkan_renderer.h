#pragma once

#include "vulkan_includes.h"

#include "vulkan_sync_wrapper.h"
#include "vulkan_swapchain_wrapper.h"
#include "vulkan_pipeline_wrapper.h"
#include "vulkan_queue_wrapper.h"
#include "vulkan_vertex_buffer.h"
#include "vulkan_uniform_buffer.h"

class VulkanRenderer
{
private:
	constexpr static bool EnableValidationLayers = false;

	VkPhysicalDevice find_suitable_gpu(const std::vector<VkPhysicalDevice>) const;
	bool does_gpu_support_req_extensions(VkPhysicalDevice) const;

public:
	// main instance
	VkInstance vkhInstance;

	// swapchain
	VulkanSwapchainWrapper swapchainWrapper;

	// pipeline
	VulkanPipelineWrapper pipelineWrapper;

	// queue
	VulkanQueueWrapper queueWrapper;

	// sync
	VulkanSyncWrapper syncWrapper;

	// vertex buffer
	VulkanVertexBuffer vertexBuffer;

	// uniform buffer
	VulkanUniformBuffer uniformBuffer;

	// physical and logical devices
	VkPhysicalDevice vkhGPU;
	VkPhysicalDeviceProperties gpuProperties;
	VkDevice vkhDevice;

	VkSurfaceKHR vkhKhrSurface;

	// sync
	VkCommandPool commandPool;
	std::vector<VkCommandBuffer> commandBuffers;

	std::vector<const char*> vpDeviceExtensions
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	std::vector<const char*> vpInstanceExtensions;
	std::vector<const char*> vpInstanceLayers;
	
	VulkanRenderer();
	~VulkanRenderer();

	bool init(GLFWwindow*);

	void on_window_change(GLFWwindow*);
	void on_window_change_cleanup();

	bool init_instance(GLFWwindow*);
	bool init_devices();
	bool init_debug_layers();

	bool render(GLFWwindow*);
	
	bool init_command_pool();
	bool update_command_buffers();
};