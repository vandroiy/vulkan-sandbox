#include "vulkan_sync_wrapper.h"

void
VulkanSyncWrapper::init(VkDevice device, VulkanSwapchainWrapper* swapchainWrapper)
{
	imageAvailableSemaphores.resize(MAX_CONCURRENT_FRAMES);
	renderFinishedSemaphores.resize(MAX_CONCURRENT_FRAMES);
	fences.resize(MAX_CONCURRENT_FRAMES);
	fencesInUse.resize(swapchainWrapper->swapchainImages.size(), VK_NULL_HANDLE);

	VkSemaphoreCreateInfo ssemaphoreCreateInfo{ VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };
	VkFenceCreateInfo fenceInfoCreateInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
	fenceInfoCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
	
	for (size_t i = 0; i < MAX_CONCURRENT_FRAMES; ++i)
	{
		VK_CHECK(vkCreateSemaphore(device, &ssemaphoreCreateInfo, nullptr, &imageAvailableSemaphores[i]));
		VK_CHECK(vkCreateSemaphore(device, &ssemaphoreCreateInfo, nullptr, &renderFinishedSemaphores[i]));
		VK_CHECK(vkCreateFence(device, &fenceInfoCreateInfo, nullptr, &fences[i]));
	}
}

void
VulkanSyncWrapper::deinit(VkDevice device)
{
	for (size_t i = 0; i < MAX_CONCURRENT_FRAMES; i++)
	{
		vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
		vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
		vkDestroyFence(device, fences[i], nullptr);
	}
}

void
VulkanSyncWrapper::advance_frame_counter()
{
	currentFrame = (currentFrame + 1) % MAX_CONCURRENT_FRAMES;
}


VkSemaphore&
VulkanSyncWrapper::get_free_image_available_semaphore()
{
	return imageAvailableSemaphores[currentFrame];
}

VkSemaphore& 
VulkanSyncWrapper::get_free_render_finished_semaphore()
{
	return renderFinishedSemaphores[currentFrame];
}

VkFence&
VulkanSyncWrapper::get_free_fence()
{
	return fences[currentFrame];
}