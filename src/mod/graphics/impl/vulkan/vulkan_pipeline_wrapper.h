#pragma once

#include "vulkan_includes.h"
#include "vulkan_swapchain_wrapper.h"

class VulkanPipelineWrapper
{
private:
	VkShaderModule create_shader_module(VkDevice, const std::vector<char>&);
	void create_descriptor_set_layouts(VkDevice);
	void create_graphics_pipeline(VkDevice, const VulkanSwapchainWrapper::SwapchainSupportDetails&);
	void create_render_pass(VkDevice, const VulkanSwapchainWrapper::SwapchainSupportDetails&);
	void create_framebuffers(VkDevice, VulkanSwapchainWrapper*);

public:
	VkShaderModule vertexShaderModule;
	VkShaderModule fragmentShaderModule;

	VkRenderPass renderPass;
	VkDescriptorSetLayout descriptorSetLayout;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;

	bool init(VkDevice, VulkanSwapchainWrapper*);

	void recreate(VkDevice, VulkanSwapchainWrapper*);
	void recreate_cleanup(VkDevice, VulkanSwapchainWrapper*);

	void deinit(VkDevice);
};