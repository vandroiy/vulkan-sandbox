#pragma once

#include "vulkan_includes.h"

class VulkanQueueWrapper
{
public:
	struct QueueFamilyIndices {
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		std::set<uint32_t> queue_families() {
			return std::set<uint32_t> {graphicsFamily.value(), presentFamily.value()};
		}
	};

	QueueFamilyIndices queueFamilyIndices;
	
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;

	VkQueue graphicsQueue;
	VkQueue presentQueue;

	bool init(VkPhysicalDevice, VkDevice, VkSurfaceKHR);
	void deinit(VkPhysicalDevice);

	void find_queue_families(VkPhysicalDevice, VkSurfaceKHR);
	void find_queue_handles(VkDevice);
};