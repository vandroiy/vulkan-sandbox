#pragma once

#include "vulkan_includes.h"

class VulkanRenderable
{
private:

public: 
    void init(VkPhysicalDevice, VkDevice, VkQueue, VkCommandPool);
    void deinit(VkDevice);

};