#pragma once 

#include "vulkan_includes.h"

class VulkanUniformBuffer
{
public:
    void init(VkPhysicalDevice, VkDevice, VkQueue, VkCommandPool);
    void deinit(VkDevice);

    void update_uniform_buffer(VkDevice, uint32_t, const VkExtent2D&);

private:
    std::vector<VkBuffer> uniformBuffers;
    std::vector<VkDeviceMemory> uniformBuffersMemory;
    
    // fixme: gigahack, 2 is the MINIMUM guaranteed 
    // image count in the swapchain, and not the actual count
    uint32_t swapchainImageCount{2};
    
    void create_uniform_buffer(VkPhysicalDevice, VkDevice, VkQueue, VkCommandPool, OUT VkBuffer&);
};