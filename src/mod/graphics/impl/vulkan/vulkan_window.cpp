#include "vulkan_window.h"

//  static member initialization
bool VulkanWindow::DEBUG = false;

VulkanWindow::VulkanWindow()
{
	pRenderer = new VulkanRenderer();
}

VulkanWindow::~VulkanWindow()
{
	if (pRenderer != nullptr)
	{
		delete pRenderer;
	}

	if (hWnd != nullptr)
	{
		glfwDestroyWindow(hWnd);
		glfwTerminate();
	}
}

bool
VulkanWindow::init()
{
	if (!glfwInit() || !glfwVulkanSupported())
	{
		return false;
	}


	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	hWnd = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, nullptr, nullptr);

	glfwSetWindowUserPointer(hWnd, this);
	glfwSetFramebufferSizeCallback(hWnd,
		[](GLFWwindow* window, int width, int height)
		{
			VulkanWindow* vulkanWindow = reinterpret_cast<VulkanWindow*>(glfwGetWindowUserPointer(window));
			if (vulkanWindow != nullptr && vulkanWindow->pRenderer != nullptr)
			{
				vulkanWindow->pRenderer->swapchainWrapper.framebufferResized = true;
			}
		}
	);

	bool rendererInitResult = pRenderer->init(hWnd);

	if (!rendererInitResult || hWnd == nullptr)
	{
		return false;
	}


	VK_CHECK_RETBOOL(glfwCreateWindowSurface(pRenderer->vkhInstance, hWnd, nullptr, &pRenderer->vkhKhrSurface));
	return true;
}

void
VulkanWindow::render()
{
	if (pRenderer != nullptr && !pRenderer->render(hWnd))
	{
		std::cout << "Rendering quit unexpectedly" << std::endl;
	}
}