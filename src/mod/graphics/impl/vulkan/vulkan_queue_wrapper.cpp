#include "vulkan_queue_wrapper.h"

bool
VulkanQueueWrapper::init(VkPhysicalDevice gpu, VkDevice device, VkSurfaceKHR surface)
{
	find_queue_families(gpu, surface);

	const float queuePriority = 1.0f;
	for (uint32_t queueFamily : queueFamilyIndices.queue_families()) {
		VkDeviceQueueCreateInfo queueCreateInfo = { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	return queueFamilyIndices.graphicsFamily.has_value() && queueFamilyIndices.presentFamily.has_value();
}

void
VulkanQueueWrapper::deinit(VkPhysicalDevice device)
{
}

void
VulkanQueueWrapper::find_queue_families(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto& queueFamily : queueFamilies) {
		if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			queueFamilyIndices.graphicsFamily = i;
		}

		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
		if (presentSupport) {
			queueFamilyIndices.presentFamily = i;
		}


		if (queueFamilyIndices.graphicsFamily.has_value() &&
			queueFamilyIndices.presentFamily.has_value()) {
			break;
		}

		i++;
	}
}

void
VulkanQueueWrapper::find_queue_handles(VkDevice device)
{
	vkGetDeviceQueue(device, queueFamilyIndices.graphicsFamily.value(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, queueFamilyIndices.presentFamily.value(), 0, &presentQueue);
}