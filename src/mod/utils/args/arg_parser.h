#pragma once

#include <vector>
#include <iostream>
#include <algorithm>

class ArgParser
{
    private:
        struct Optional
        {
            std::string shortName;
            std::string longName;
            bool* optionalReference;
        };

        std::vector<Optional> optionalFlags;

    public: 
        void add_optional(std::string shortName, std::string longName, bool* optionalReference) {
            Optional optional;
            optional.shortName = shortName;
            optional.longName = longName;
            optional.optionalReference = optionalReference;

            optionalFlags.push_back(optional);
        }
        
        void parse(int argc, char *argv[]) {
            std::vector<std::string> arguments;
            for (int i = 0; i < argc; ++i) {
                arguments.push_back(argv[i]);
            }

            for (auto& optional : optionalFlags) {
                const auto shortIt = std::find(arguments.begin(), arguments.end(), optional.shortName);
                const auto longIt = std::find(arguments.begin(), arguments.end(), optional.longName);

                if (optional.optionalReference != nullptr) {
                    *(optional.optionalReference) = shortIt != arguments.end() || longIt != arguments.end();
                }
            }
        }
};