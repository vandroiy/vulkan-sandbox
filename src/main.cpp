#include "mod/all.h"

int main(int argc, char* argv[])
{
    ArgParser argParser;
    argParser.add_optional("-d", "-debug", &VulkanWindow::DEBUG);
    argParser.parse(argc, argv);

    VulkanWindow vulkanWindow;
    if (!vulkanWindow.init())
    {
        std::cout << "Failed to initialize Vulkan context" << std::endl;
        abort();
    }

    vulkanWindow.render();
    
    return 0;
}
