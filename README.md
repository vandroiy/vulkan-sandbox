### BUILD
I'm only testing shit on Windows, so usually opening the project in Visual Studio (2019) does the job. Select `vai.vcxproj` as the startup project, then select either Debug or Release. Hitting the build button should build the project, and running the project should run it : )

### Adding shaders
Under the `shaders` folder, there is a batch script that compiles the shaders to SPIR-V, then copies them to `bin/Release` and `bin/Debug`. So don't forget about that script.

